<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png" /> -->
    <!-- Responsive and mobile friendly stuff -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--jQuery-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.2.min.js"></script>

    <!--Styles-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css" />  

    <!--prefix free-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/prefixfree.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.5.3-min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>    

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->   

	<title><?php echo get_bloginfo("name"); ?></title>

<?php wp_head(); ?>
</head>
<body>
<div id="wrapper" class="container">
    <header id="header" class="inner">
        <div id="header-top" class="section group">
            <div id="logo" class="col span_1_3">
                <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" title="<?php echo get_bloginfo('name'); ?>" alt="<?php echo get_bloginfo('name'); ?>"></a>
            </div> 
            <?php
                $options = get_option( 'APF_AddForms' );
                $slogan = $options['theme_settings']["theme_settings_form"]["slogan"];
            ?>                
            <div id="slogan" class="col span_1_3"><?php echo $slogan[0] ?></div>  
            <div id="social-network" class="col span_1_3">
                <ul>
                    <?php
                        $social_media_links = $options['theme_settings']["theme_settings_form"]["social_media_form"];
                    ?>
                    <?php if (!empty($social_media_links[0])): ?><li class='facebook'><a href='<?php echo $social_media_links[0] ?>' target='_blank'>Facebook</a></li><?php endif ?>
                    <?php if (!empty($social_media_links[1])): ?><li class='twitter'><a href='<?php echo $social_media_links[1] ?>' target='_blank'>Twitter</a></li><?php endif ?>
                    <?php if (!empty($social_media_links[2])): ?><li class='linkedin'><a href='<?php echo $social_media_links[2] ?>' target='_blank'>LinkedIn</a></li><?php endif ?>
                </ul>
            </div>
        </div>  
        <nav class="section group">
            <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            <?php if ( is_user_logged_in() ) 
                    wp_nav_menu( array( 'theme_location' => 'logout' ) );
                  else
                    wp_nav_menu( array( 'theme_location' => 'member' ) );  ?>
        </nav>   
    </header>

    <script type="text/javascript">
    $(function () {
        if ($('#col-submenu2').length > 0){
            $('.sub-menu2').css('width','576px');
        }
      if ($('#col-submenu4').length > 0){
            $('.sub-menu3').css('width','576px');
        }        
    });
    </script>