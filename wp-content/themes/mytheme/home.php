<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
    <section id="slider">
        <?php 
        $args = array('post_type' => array('slider'));  
        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ):
        ?>
            <div id="slider-wrapper" class="section group">
                <div id="slideshow" class="flexslider col span_2_2">
                    <ul class="slides">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php if (has_post_thumbnail()): ?>
                        <li>
                            <?php if (has_post_thumbnail()) the_post_thumbnail('homepage-slider'); ?>                             
                            <div class="text inner">
                                <h1><?php the_title(); ?></h1>
                                <div class="more"><a href="<?php the_permalink(); ?>">Read More</a></div>
                            </div>                                                                     
                        </li>
                        <?php endif ?>
                    <?php endwhile; ?>
                    </ul>
                </div>
            </div>    
            <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
            <script type="text/javascript" charset="utf-8">
                    jQuery('#slideshow').flexslider({
                        animation: "fade",
                        direction: "horizontal",
                        slideshowSpeed: 7000,
                        animationSpeed: 600,
                        directionNav: true,
                        controlNav: false,
                        initDelay: 0,
                        prevText: "",           
                        nextText: "",
                        pauseOnHover: true,
                    });
            </script>
        <?php endif ?>  
    </section>
    <div id="home-content" class="section group inner">
        <div id="boxes-container">
            <?php
            $args = array('post_type' => array('box'), 'posts_per_page' => 3);
            $the_query = new WP_Query( $args );
            $i=0;

            while ( $the_query->have_posts() ) : $the_query->the_post(); $i++;
            ?>          
                <div class="box-container col span_1_3">
                    <section class="section<?php echo $i ?>">                 
                        <div class="box-image" id="box-image<?php echo $i ?>"></div> 
                        <h2><?php the_title(); ?></h2>
                        <div class="text"><?php the_content(); ?></div>
                        <div class="more"><a href="<?php the_permalink(); ?>">Read More</a></div>
                    </section>
                </div>
            <?php endwhile ?>
        </div>
    </div>  

<script type="text/javascript">
function equalHeight(group) {
    var tallest = 0;
    group.each(function() {
        var thisHeight = $(this).height();
            if(thisHeight > tallest) {
                tallest = thisHeight;
            }
    });
    group.height(tallest+75);
}
$(document).ready(function() {
    equalHeight($(".box-container section"));
});     
</script>    

<?php get_footer(); ?>    