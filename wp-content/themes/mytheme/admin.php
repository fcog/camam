<?php

/************* ADMIN PAGE *****************/

// Include the library class
if ( !class_exists( 'AdminPageFramework' ) )
    include_once( dirname( __FILE__ ) . '/class/admin-page-framework.php' );
 
// extend the class
class APF_AddForms extends AdminPageFramework {
 
    // Define the setup method to set how many pages, page titles and icons etc.
    public function setUp() {
       
        // Create the root menu
        $this->setRootMenuPage( 'Settings' );        // specifies to which parent menu to add.
        // Dashboard, Posts, Media, Links, Pages, Comments, Appearance, Plugins, Users, Tools, Settings, Network Admin
       
        // Add the sub menus and the pages
        $this->addSubMenuPage(   
            'Theme Settings',        // the page and menu title
            'theme_settings'         // the page slug
        );    
 
        // Add form sections.
        $this->addSettingSections(
            array(
                'strSectionID'    => 'theme_settings_form',    // the section ID
                'strPageSlug'    => 'theme_settings',    // the page slug that the section belongs to
                'strTitle'    => 'Homepage Settings',    // the section title
            )
        );       
        $this->addSettingFields(
            array(  // Multiple text fields
              'strFieldID' => 'social_media_form',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Social Media Links', 'my-theme' ),
              'strType' => 'text',
              'vLabel' => array( 
                'Facebook: ', 
                'Twitter: ', 
                'LinkedIn: ',
              ),
              'vSize' => array(
                40,
                40,
                40,
              ),
              'vDelimiter' => '<br />',
            ),       
            array(  // Text Area
              'strFieldID' => 'slogan',
              'strSectionID' => 'theme_settings_form',
              'strType' => 'textarea',
              'vLabel' => array( 
                'Slogan text: ', 
              ),
              'vRows' => 3,
              'vCols' => 40,
            ),    
            array( // Media Files
              'strFieldID' => 'logos',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Partners Logos', 'admin-page-framework-demo' ),
              'strType' => 'media',
              'fRepeatable' => true,
            ),                                                                                           
            array(  // Multiple text fields
              'strFieldID' => 'copyright_text',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Footer Copyright text', 'my-theme' ),
              'strType' => 'text',
              'vLabel' => array( 
                'Copyright text: ', 
              ),
              'vSize' => array(
                70,
              ),
              'vDelimiter' => '<br />',
            ),     
            array(  // Multiple text fields
              'strFieldID' => 'location_text',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Footer Location text', 'my-theme' ),
              'strType' => 'text',
              'vLabel' => array( 
                'Location text: ', 
              ),
              'vSize' => array(
                40,
              ),
              'vDelimiter' => '<br />',
            ),                                                             
            array( // Submit button
                'strFieldID' => 'submit_button',
                'strSectionID' => 'theme_settings_form',
                'strType' => 'submit',
                'vLabel' => __( 'Save Changes' ),
            )
        );                 
    }
   
    // public function do_theme_settings() {    // do_ + page slug   
                   
    //     // Show the saved option value.
    //     // The extended class name is used as the option key. This can be changed by passing a custom string to the constructor.
    //     echo '<h3>Saved Values</h3>';
    //     echo $this->oDebug->getArray( get_option( 'APF_AddForms' ) );
       
    // }
   
}
 
// Instantiate the class object.
if ( is_admin() )
    new APF_AddForms;