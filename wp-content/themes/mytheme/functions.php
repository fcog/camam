<?php

require get_template_directory() . '/admin.php';

function mytheme_setup() {
    register_nav_menu( 'primary', __( 'Navigation Menu', 'mytheme' ) );
    register_nav_menu( 'member', __( 'Member Menu', 'mytheme' ) );
	register_nav_menu( 'logout', __( 'Logout Menu', 'mytheme' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
    add_image_size( 'homepage-slider', 1900, 577, true ); 
}
add_action( 'after_setup_theme', 'mytheme_setup' );

// function add_class_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'primary' )
//   		$output = preg_replace('/class="menu-item/', '', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_class_to_menu_items', 10, 2);


// function add_div_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'footer' )
//   		$output = preg_replace('/<a href/', '<div class="arrow-right"></div><a href', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_div_to_menu_items', 10, 2);

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function mytheme_widgets_init() {

/************* HEADER *********************/   

    register_sidebar( array(
      'id'            => 'sidebar-blog',
      'name'          => __( 'Sidebar - Single Post', 'mytheme' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'sidebar-category',
      'name'          => __( 'Sidebar - Categories', 'mytheme' ),
      'before_widget'  => '',                  
    ) );      
}
add_action( 'widgets_init', 'mytheme_widgets_init' );


/************* HELPER FUNCTIONS *****************/

//Remove menu div container
function prefix_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}
add_filter('wp_nav_menu_args', 'prefix_nav_menu_args');

add_filter('wp_nav_menu_items', 'add_projects', 10, 2);
function add_projects($items, $args) {
    // echo(var_dump($args));
  if ($args->theme_location == "primary"){
      $cat1 = '3'; // define category
      $cat2 = '4'; // define category
      $current_projects = array();
      $future_projects = array();
      $current_projects = get_posts("cat=$cat1");
      $future_projects = get_posts("cat=$cat2");
      $menu = explode('Projects', $items);
      // echo var_dump($menu);
      $menuOut = $menu[0];
      $menuOut .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20a"><a href="#">Projects</a>';

      if ($current_projects[0]->post_title != '' || $future_projects[0]->post_title != '') {
          $menuOut .= '<div class="sub-menu2">';
      }

      if ($current_projects[0]->post_title != '') {
          $menuOut .= "<div class='col-submenu' id='col-submenu1'><h2>Current Projects</h2>";
          foreach ( $current_projects as $current_project ) {
            $permalink = get_permalink( $current_project->ID );
            $menuOut .= '<div class="row"><a href="'.$permalink.'">'.$current_project->post_title.'</a></div>';
          }
          $menuOut .= "</div>";
      }
      if ($future_projects[0]->post_title != '') {
          $menuOut .= "<div class='col-submenu' id='col-submenu2'><h2>Future Projects</h2>";
          foreach ( $future_projects as $future_project ) {
            $permalink = get_permalink( $future_project->ID );
            $menuOut .= '<div class="row"><a href="'.$permalink.'">'.$future_project->post_title.'</a></div>';
          }
          $menuOut .= "</div>";
      }
      if ($current_projects[0]->post_title != '' || $future_projects[0]->post_title != '') {   
          $menuOut .= '</div>';
      }
      $menuOut .= "</li>";
      $menuOut .= $menu[1];
      return $menuOut;
  }
  else{
    return $items;
  }
}

add_filter('wp_nav_menu_items', 'add_events', 10, 2);
function add_events($items, $args) {
    // echo(var_dump($args));
  if ($args->theme_location == "primary"){
      $cat1 = '7'; // recent events
      $cat2 = '8'; // upcoming events
      $recent_events = array();
      $upcoming_events = array();
      $recent_events = get_posts("cat=$cat1");
      $upcoming_events = get_posts("cat=$cat2");
      $menu = explode('Events', $items);
      // echo var_dump($menu);
      $menuOut = $menu[0];
      $menuOut .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20b"><a href="#">Events</a>';

      if ($recent_events[0]->post_title != '' || $upcoming_events[0]->post_title != '') {
          $menuOut .= '<div class="sub-menu3">';
      }

      if ($recent_events[0]->post_title != '') {
          $menuOut .= "<div class='col-submenu' id='col-submenu3'><h2>Recent Events</h2>";
          foreach ( $recent_events as $recent_event ) {
            $permalink = get_permalink( $recent_event->ID );
            $menuOut .= '<div class="row"><a href="'.$permalink.'">'.$recent_event->post_title.'</a></div>';
          }
          $menuOut .= "</div>";
      }
      if ($upcoming_events[0]->post_title != '') {
          $menuOut .= "<div class='col-submenu' id='col-submenu4'><h2>Upcoming Events</h2>";
          foreach ( $upcoming_events as $upcoming_event ) {
            $permalink = get_permalink( $upcoming_event->ID );
            $menuOut .= '<div class="row"><a href="'.$permalink.'">'.$upcoming_event->post_title.'</a></div>';
          }
          $menuOut .= "</div>";
      }
      if ($recent_events[0]->post_title != '' || $upcoming_events[0]->post_title != '') {   
          $menuOut .= '</div>';
      }
      $menuOut .= "</li>";
      $menuOut .= $menu[1];
      return $menuOut;
  }
  else{
    return $items;
  }
}

// add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2);
// function add_login_logout_link($items, $args) {
//     if ($args->theme_location == "logout"){
//         ob_start();
//         wp_loginout(site_url());
//         $loginoutlink = ob_get_contents();
//         ob_end_clean();
//         $items .= '<li>'. $loginoutlink .'</li>';
//     }
//     return $items;
// }

add_action( 'init', 'register_cpt_slider' );
function register_cpt_slider() {
    $labels = array(
    'name' => _x( 'Sliders', 'slider' ),
    'singular_name' => _x( 'Slider', 'slider' ),
    'add_new' => _x( 'Add New', 'slider' ),
    'add_new_item' => _x( 'Add New Slider', 'slider' ),
    'edit_item' => _x( 'Edit Slider', 'slider' ),
    'new_item' => _x( 'New Slider', 'slider' ),
    'view_item' => _x( 'View Slider', 'slider' ),
    'search_items' => _x( 'Search Sliders', 'slider' ),
    'not_found' => _x( 'No sliders found', 'slider' ),
    'not_found_in_trash' => _x( 'No sliders found in Trash', 'slider' ),
    'parent_item_colon' => _x( 'Parent Slider:', 'slider' ),
    'menu_name' => _x( 'Sliders', 'slider' ),
    );
    $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'Homepage slider posts',
    'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 20,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
    );
    register_post_type( 'slider', $args );
} 

add_action( 'init', 'register_cpt_box' );
function register_cpt_box() {
    $labels = array(
    'name' => _x( 'Box', 'box' ),
    'singular_name' => _x( 'Box', 'box' ),
    'add_new' => _x( 'Add New', 'box' ),
    'add_new_item' => _x( 'Add New Box', 'box' ),
    'edit_item' => _x( 'Edit Box', 'box' ),
    'new_item' => _x( 'New Box', 'box' ),
    'view_item' => _x( 'View Box', 'box' ),
    'search_items' => _x( 'Search Boxes', 'box' ),
    'not_found' => _x( 'No boxes found', 'box' ),
    'not_found_in_trash' => _x( 'No boxs found in Trash', 'box' ),
    'parent_item_colon' => _x( 'Parent Box:', 'box' ),
    'menu_name' => _x( 'Boxes', 'box' ),
    );
    $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'Homepage box posts',
    'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 20,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
    );
    register_post_type( 'box', $args );
} 


function change_wp_login_title() {
  if (isset($_GET['role'])){
    $user_type = $_GET['role']; 
    switch ($user_type) {
      case 'advantage-individual':
        ?>
        <h1 class="login-title inner">Advantage Individual Membership USD $150</h1>
        <?php 
        break;
      case 'bronze-corporate':
        ?>
        <h1 class="login-title inner">Bronze Corporate Membership USD $500</h1>
        <?php 
        break;
      case 'silver-corporate':
        ?>
        <h1 class="login-title inner">Silver Corporate Membership USD $1000</h1>
        <?php 
        break;
      case 'gold-corporate':
        ?>
        <h1 class="login-title inner">Gold Corporate Membership USD $1500</h1>
        <?php 
        break;                    
      default:
        ?>
        <h1 class="login-title inner">FREE Individual Membership</h1>
        <?php
        break;
    }
  }
}
add_filter('login_headertitle', 'change_wp_login_title');