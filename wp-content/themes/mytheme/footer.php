    <footer id="footer">
        <?php $options = get_option( 'APF_AddForms' ); ?>
        <div id="footer-top" class="section group inner">
            <?php $logos = $options['theme_settings']["theme_settings_form"]["logos"]; ?>
            <?php foreach ($logos as $key => $value): ?>
                <img src="<?php echo $logos[$key] ?>">
            <?php endforeach ?>
        </div>
        <div id="footer-bottom" class="section group inner">
            <?php $copyright_text = $options['theme_settings']["theme_settings_form"]["copyright_text"]; ?>
            <?php $location_text = $options['theme_settings']["theme_settings_form"]["location_text"]; ?>
            <?php $social_media_links = $options['theme_settings']["theme_settings_form"]["social_media_form"]; ?>
            <section id="copyright">
                <?php echo $copyright_text[0] ?> <span>|</span> <?php echo $location_text[0] ?> <span>|</span> Share: 
                <ul>
                    <?php
                        $social_media_links = $options['theme_settings']["theme_settings_form"]["social_media_form"];
                    ?>
                    <?php if (!empty($social_media_links[0])): ?><li class='facebook'><a href='<?php echo $social_media_links[0] ?>' target='_blank'>Facebook</a></li><?php endif ?>
                    <?php if (!empty($social_media_links[1])): ?><li class='twitter'><a href='<?php echo $social_media_links[1] ?>' target='_blank'>Twitter</a></li><?php endif ?>
                    <?php if (!empty($social_media_links[2])): ?><li class='linkedin'><a href='<?php echo $social_media_links[2] ?>' target='_blank'>LinkedIn</a></li><?php endif ?>
                </ul>
            </section>   
        </div>
    </footer>
</div>
<?php wp_footer() ?>
</body>
</html>