<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<!-- <div class="entry-thumbnail"> -->
			<?php //the_post_thumbnail('header-image'); ?>
		<!-- </div> -->
		<?php //endif; ?>

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<?php if (get_post_type() != 'page'): ?>
		<p class="entry-date">
			<!-- <?php //_e('Posted') ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author"><?php the_author() ?></a> <time><?php the_time('d F Y'); ?> г.</time> -->
		</p>
		<?php endif ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
		<a href="<?php echo get_permalink(); ?>">Подробнее</a>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'orji' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php comments_template( '', true ); ?>

	</footer><!-- .entry-meta -->
</article><!-- #post -->
